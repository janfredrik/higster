package no.clap.higster.test;

import com.robotium.solo.*;
import android.test.ActivityInstrumentationTestCase2;


@SuppressWarnings("rawtypes")
public class bookStoreTest extends ActivityInstrumentationTestCase2 {
  	private Solo solo;
  	
  	private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "no.clap.higster.MainActivity";

    private static Class<?> launcherActivityClass;
    static{
        try {
            launcherActivityClass = Class.forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
        } catch (ClassNotFoundException e) {
           throw new RuntimeException(e);
        }
    }
  	
  	@SuppressWarnings("unchecked")
    public bookStoreTest() throws ClassNotFoundException {
        super(launcherActivityClass);
    }

  	public void setUp() throws Exception {
        super.setUp();
		solo = new Solo(getInstrumentation());
		getActivity();
  	}
  
   	@Override
   	public void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
  	}
  
	public void testRun() {
        //Wait for activity: 'no.clap.higster.MainActivity'
		solo.waitForActivity("MainActivity", 2000);
        //Click on Bookstore
		solo.clickOnView(solo.getView("bookstore"));
        //Wait for activity: 'no.clap.higster.FindBooksActivity'
		assertTrue("FindBooksActivity is not found!", solo.waitForActivity("FindBooksActivity"));
        //Assert that: 'The bookstore, which is Gjøviks foremost in literature, whose main t' is shown
		assertTrue("'The bookstore, which is Gjøviks foremost in literature, whose main t' is not shown!", solo.waitForView(solo.getView("textView2")));
        //Click on Empty Text View
		solo.clickOnView(solo.getView("autocomplete_class"));
        //Enter the text: '13'
		solo.clearEditText((android.widget.EditText) solo.getView("autocomplete_class"));
		solo.enterText((android.widget.EditText) solo.getView("autocomplete_class"), "13");
        //Click on 13HBWUA
		solo.clickOnView(solo.getView(android.R.id.text1, 2));
        //Enter the text: '13HBWUA'
		solo.clearEditText((android.widget.EditText) solo.getView("autocomplete_class"));
		solo.enterText((android.widget.EditText) solo.getView("autocomplete_class"), "13HBWUA");
        //Click on Databaser, Kjell Toft Hansen / Tore Mallaug, Tisip, utgave 2
		solo.clickOnWebElement(By.textContent("Databaser, Kjell Toft Hansen / Tore Mallaug, Tisip, utgave 2"));
	}
}
