package no.clap.higster.test;

import com.robotium.solo.*;
import android.test.ActivityInstrumentationTestCase2;


@SuppressWarnings("rawtypes")
public class OpeningHoursAndHelpdeskTest extends ActivityInstrumentationTestCase2 {
  	private Solo solo;
  	
  	private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "no.clap.higster.MainActivity";

    private static Class<?> launcherActivityClass;
    static{
        try {
            launcherActivityClass = Class.forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
        } catch (ClassNotFoundException e) {
           throw new RuntimeException(e);
        }
    }
  	
  	@SuppressWarnings("unchecked")
    public OpeningHoursAndHelpdeskTest() throws ClassNotFoundException {
        super(launcherActivityClass);
    }

  	public void setUp() throws Exception {
        super.setUp();
		solo = new Solo(getInstrumentation());
		getActivity();
  	}
  
   	@Override
   	public void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
  	}
  
	public void testRun() {
        //Wait for activity: 'no.clap.higster.MainActivity'
		solo.waitForActivity("MainActivity", 2000);
        //Click on Opening hours
		solo.clickOnView(solo.getView("openingHours"));
        //Wait for dialog
		solo.waitForDialogToOpen(5000);
        //Assert that: 'Opening hours' is shown
		assertTrue("'Opening hours' is not shown!", solo.waitForView(solo.getView("sdl__title")));
        //Assert that: ' Student centre: Mon. - Fri.: 08.00-15.30  Library: Mon. - Sun.: 06.' is shown
		assertTrue("' Student centre: Mon. - Fri.: 08.00-15.30  Library: Mon. - Sun.: 06.' is not shown!", solo.waitForView(solo.getView("sdl__message")));
        //Click on Close
		solo.clickOnView(solo.getView("sdl__positive_button"));
        //Click on Helpdesk
		solo.clickOnView(solo.getView("helpdesk"));
        //Wait for activity: 'no.clap.higster.HelpdeskActivity'
		assertTrue("HelpdeskActivity is not found!", solo.waitForActivity("HelpdeskActivity"));
        //Click on glyphicon glyphicon-search
		solo.clickOnWebElement(By.className("glyphicon glyphicon-search"));
        //Click on glyphicon glyphicon-search
		solo.clickOnWebElement(By.className("glyphicon glyphicon-search"));
        //Press menu back key
		solo.goBack();
	}
}
