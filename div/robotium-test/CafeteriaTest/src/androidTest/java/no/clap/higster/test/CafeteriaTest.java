package no.clap.higster.test;

import com.robotium.solo.*;
import android.test.ActivityInstrumentationTestCase2;


@SuppressWarnings("rawtypes")
public class CafeteriaTest extends ActivityInstrumentationTestCase2 {
  	private Solo solo;
  	
  	private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "no.clap.higster.MainActivity";

    private static Class<?> launcherActivityClass;
    static{
        try {
            launcherActivityClass = Class.forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
        } catch (ClassNotFoundException e) {
           throw new RuntimeException(e);
        }
    }
  	
  	@SuppressWarnings("unchecked")
    public CafeteriaTest() throws ClassNotFoundException {
        super(launcherActivityClass);
    }

  	public void setUp() throws Exception {
        super.setUp();
		solo = new Solo(getInstrumentation());
		getActivity();
  	}
  
   	@Override
   	public void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
  	}
  
	public void testRun() {
        //Wait for activity: 'no.clap.higster.MainActivity'
		solo.waitForActivity("MainActivity", 2000);
        //Click on Cafeteria
		solo.clickOnView(solo.getView("cafeteria"));
        //Wait for activity: 'no.clap.higster.CafeteriaActivity'
		assertTrue("CafeteriaActivity is not found!", solo.waitForActivity("CafeteriaActivity"));
        //Assert that: 'The Student cafeterias are a natural meeting place for students at c' is shown
		assertTrue("'The Student cafeterias are a natural meeting place for students at c' is not shown!", solo.waitForView(solo.getView("tv_description")));
        //Assert that: 'Dinner menu this week:' is shown
		assertTrue("'Dinner menu this week:' is not shown!", solo.waitForView(solo.getView("tv_weekTitle")));
        //Click on Monday Crunchy chicken Tikka Masala m/ris Fiskegryte m/indisk smak Jalfrezi
		solo.clickInList(1, 0);
        //Click on Tuesday Beefburger m/pommes frites Fisketallerken
		solo.clickInList(2, 0);
        //Click on Wednesday Spaghetti Bolognaise Uerfilet m/eggesmør
		solo.clickInList(3, 0);
        //Click on Thursday Kjøtt og makaronigrateng Stekt fiskepudding m/råkost
		solo.clickInList(4, 0);
        //Click on Friday Kokkens varmlunsj
		solo.clickInList(5, 0);
        //Assert that: '08.00 - 17.00 (Mon-Thu), 08.00 - 15.00 (Fri)' is shown
		assertTrue("'08.00 - 17.00 (Mon-Thu), 08.00 - 15.00 (Fri)' is not shown!", solo.waitForView(solo.getView("tv_openingHors")));
        //Click on action bar item
		solo.clickOnActionBarItem(0x7f080095);
        //Wait for dialog
		solo.waitForDialogToOpen(5000);
        //Assert that: 'Notifications' is shown
		assertTrue("'Notifications' is not shown!", solo.waitForView(solo.getView("sdl__title")));
        //Assert that: 'Higster will send you a notification when a new dinner list from the' is shown
		assertTrue("'Higster will send you a notification when a new dinner list from the' is not shown!", solo.waitForView(solo.getView("sdl__message")));
        //Click on Yes
		solo.clickOnView(solo.getView("sdl__positive_button"));
        //Wait for dialog
		solo.waitForDialogToOpen(5000);
        //Click on OK
		solo.clickOnView(solo.getView("confirm_button"));
        //Click on action bar item
		solo.clickOnActionBarItem(0x7f080095);
        //Wait for dialog
		solo.waitForDialogToOpen(5000);
        //Click on No
		solo.clickOnView(solo.getView("sdl__negative_button"));
        //Wait for dialog
		solo.waitForDialogToOpen(5000);
        //Click on OK
		solo.clickOnView(solo.getView("confirm_button"));
        //Click on HomeView SOPP Cafeteria
		solo.clickOnView(solo.getView(android.widget.LinearLayout.class, 5));
        //Wait for activity: 'no.clap.higster.MainActivity'
		assertTrue("MainActivity is not found!", solo.waitForActivity("MainActivity"));
        //Click on Cafeteria
		solo.clickOnView(solo.getView("cafeteria"));
        //Wait for activity: 'no.clap.higster.CafeteriaActivity'
		assertTrue("CafeteriaActivity is not found!", solo.waitForActivity("CafeteriaActivity"));
	}
}
