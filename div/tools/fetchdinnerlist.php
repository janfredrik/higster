<?php
header('Content-Type: text/html; charset=utf-8');

// example of how to use basic selector to retrieve HTML contents
include('simple_html_dom.php');
 
// get DOM from URL or file
$html = file_get_html('http://www.sopp.no/spisesteder-gjovik');

$output = '{ "dinner" : [';

foreach($html->find('p[style=text-align: left;]') as $e) {
    echo $e->plaintext . '<br>';
	
	if (substr( $e->plaintext, 0, 6 ) === "Mandag") {
	  $output .= '{ "day" : "Monday",';
	  $output .= '"food" : "'.substr( $e->plaintext, 9, strlen($e->plaintext)) .'" },';
	}
	
	else if (substr( $e->plaintext, 0, 7 ) === "Tirsdag") {
	  $output .= '{ "day" : "Tuesday",';
	  $output .= '"food" : "'.substr( $e->plaintext, 10, strlen($e->plaintext)) .'" },';
	}
	
	else if (substr( $e->plaintext, 0, 6 ) === "Onsdag") {
	  $output .= '{ "day" : "Wednesday",';
	  $output .= '"food" : "'.substr( $e->plaintext, 10, strlen($e->plaintext)) .'" },';
	}
	
	else if (substr( $e->plaintext, 0, 7 ) === "Torsdag") {
	  $output .= '{ "day" : "Thursday",';
	  $output .= '"food" : "'.substr( $e->plaintext, 11, strlen($e->plaintext)) .'" },';
	}
	
	else if (substr( $e->plaintext, 0, 6 ) === "Fredag") {
	  $output .= '{ "day" : "Friday",';
	  $output .= '"food" : "'.substr( $e->plaintext, 10, strlen($e->plaintext)) .'" } ] }';
	}
}

$output = str_replace(array("\r\n", "\r", "\n"), " ", $output);

$fh = fopen("dinner.json", 'w');
fwrite($fh, prettyPrint($output));
fclose($fh);


function prettyPrint( $json )
{
    $result = '';
    $level = 0;
    $in_quotes = false;
    $in_escape = false;
    $ends_line_level = NULL;
    $json_length = strlen( $json );

    for( $i = 0; $i < $json_length; $i++ ) {
        $char = $json[$i];
        $new_line_level = NULL;
        $post = "";
        if( $ends_line_level !== NULL ) {
            $new_line_level = $ends_line_level;
            $ends_line_level = NULL;
        }
        if ( $in_escape ) {
            $in_escape = false;
        } else if( $char === '"' ) {
            $in_quotes = !$in_quotes;
        } else if( ! $in_quotes ) {
            switch( $char ) {
                case '}': case ']':
                    $level--;
                    $ends_line_level = NULL;
                    $new_line_level = $level;
                    break;

                case '{': case '[':
                    $level++;
                case ',':
                    $ends_line_level = $level;
                    break;

                case ':':
                    $post = " ";
                    break;

                case " ": case "\t": case "\n": case "\r":
                    $char = "";
                    $ends_line_level = $new_line_level;
                    $new_line_level = NULL;
                    break;
            }
        } else if ( $char === '\\' ) {
            $in_escape = true;
        }
        if( $new_line_level !== NULL ) {
            $result .= "\n".str_repeat( "\t", $new_line_level );
        }
        $result .= $char.$post;
    }

    return $result;
}

?>