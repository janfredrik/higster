package no.clap.higster;

import android.test.ActivityInstrumentationTestCase;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Lars Dølvik on 25.11.2014.
 */
public class CafeteriaActivityTest extends ActivityInstrumentationTestCase2<CafeteriaActivity> {

    CafeteriaActivity activity;

    public CafeteriaActivityTest(){
        super(CafeteriaActivity.class);
    }

    @Override
    protected void setUp() throws Exception{
        super.setUp();
        activity = getActivity();
    }
    @SmallTest
    public void testListViewNotNull(){
        ListView dinnerList = (ListView) activity.findViewById(R.id.dinnerList);
        assertNotNull(dinnerList);
    }
    @SmallTest
    public void testTextViewNotNull() {
        TextView tv_description = (TextView) activity.findViewById(R.id.tv_description);
        assertNotNull(tv_description);
    }
    @SmallTest
    public void testTextView2NotNull() {
        TextView tv_openingHours = (TextView) activity.findViewById(R.id.tv_openingHours);
        assertNotNull(tv_openingHours);
    }
    public void testTextViewWeekTitle() {
        TextView tv_weekTitle = (TextView) activity.findViewById(R.id.tv_weekTitle);
        assertNotNull(tv_weekTitle);
    }
}
