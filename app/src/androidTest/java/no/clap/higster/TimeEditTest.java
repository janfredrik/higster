package no.clap.higster;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.TextView;

/**
 * Created by Lars Dølvik on 02.12.2014.
 */
public class TimeEditTest extends ActivityInstrumentationTestCase2<TimeEdit> {

    TimeEdit activity;

    public TimeEditTest() {super(TimeEdit.class);}

    @Override
    protected void setUp() throws Exception{
        super.setUp();
        activity = getActivity();
    }
    @SmallTest
    public void testTextView4(){
        TextView textView4 = (TextView) activity.findViewById(R.id.textView4);
        assertNotNull(textView4);
    }
    @SmallTest
    public void testTextViewInterval(){
        TextView tv_interval = (TextView) activity.findViewById(R.id.tv_interval);
        assertNotNull(tv_interval);
    }
}
