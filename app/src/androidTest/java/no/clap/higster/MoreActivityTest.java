package no.clap.higster;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.ListView;

/**
 * Created by Lars Dølvik on 02.12.2014.
 */
public class MoreActivityTest extends ActivityInstrumentationTestCase2<MoreActivity> {

    MoreActivity activity;

    public MoreActivityTest() {super(MoreActivity.class);}

    @Override
    protected void setUp() throws Exception{
        super.setUp();
        activity = getActivity();
    }
    @SmallTest
    public void testListViewShowing(){
        ListView listView = (ListView) activity.findViewById(R.id.listView);
        assertNotNull(listView);
    }
}
