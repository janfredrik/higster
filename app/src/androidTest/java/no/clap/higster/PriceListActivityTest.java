package no.clap.higster;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.ListView;

/**
 * Created by Lars Dølvik on 02.12.2014.
 */
public class PriceListActivityTest extends ActivityInstrumentationTestCase2<PriceListActivity> {

    PriceListActivity activity;

    public PriceListActivityTest() {super(PriceListActivity.class);}

    @Override
    protected void setUp() throws Exception{
        super.setUp();
        activity = getActivity();
    }
    @SmallTest
    public void testListViewIsShowing(){
        ListView lvExp = (ListView) activity.findViewById(R.id.lvExp);
        assertNotNull(lvExp);
    }
}
