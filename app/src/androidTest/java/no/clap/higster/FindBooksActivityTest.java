package no.clap.higster;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.TextView;

/**
 * Created by Lars Dølvik on 02.12.2014.
 */
public class FindBooksActivityTest extends ActivityInstrumentationTestCase2<FindBooksActivity> {

    FindBooksActivity activity;

    public FindBooksActivityTest(){
        super(FindBooksActivity.class);
    }

    @Override
    protected void setUp() throws Exception{
        super.setUp();
        activity = getActivity();
    }
    @SmallTest
    public void testTextView2NotNull(){
        TextView textView2 = (TextView) activity.findViewById(R.id.textView2);
        assertNotNull(textView2);
    }
    @SmallTest
    public void testTextView3NotNull(){
        TextView textView3 = (TextView) activity.findViewById(R.id.textView3);
        assertNotNull(textView3);
    }
}