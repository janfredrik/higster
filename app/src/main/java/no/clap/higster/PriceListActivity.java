package no.clap.higster;

import no.clap.higster.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.Toast;

public class PriceListActivity extends Activity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_list);

        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.lvExp);

        // preparing list data
        prepareListData();

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);
    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("Kaffeprodukter");
        listDataHeader.add("Drikkevarer");
        listDataHeader.add("Brød/bakevarer");
        listDataHeader.add("Kaker");
        listDataHeader.add("Småanretninger");
        listDataHeader.add("Diverse");
        listDataHeader.add("Middager/lunsj");

        // Adding child data
        List<String> kaffeprodukter = new ArrayList<String>();
        kaffeprodukter.add("Kaffe/te        8,- 12,- 15,-");
        kaffeprodukter.add("Kaffekort 20 klipp      120,-");
        kaffeprodukter.add("Termokopp       95,-");
        kaffeprodukter.add("Varm Sjokolade      12,- 15,- 20,-");

        List<String> drikkevarer = new ArrayList<String>();
        drikkevarer.add("Melk 0,25l      6,-");
        drikkevarer.add("Milkshake      22,-");
        drikkevarer.add("Litago 0,5l        22,-");
        drikkevarer.add("Iste/iskaffe        20,-");
        drikkevarer.add("Iste på flaske 0,5l        30,-");
        drikkevarer.add("Juice 0,25l        12,-");
        drikkevarer.add("Juiceflaske        22,-");
        drikkevarer.add("Mineralvann        22,-");
        drikkevarer.add("0,5l vann u/kullsyre        10,-");
        drikkevarer.add("0,7l vann med sportscap        28,-");
        drikkevarer.add("Vitamin Well/Bramhulst        35,-");

        List<String> bakevarer = new ArrayList<String>();
        bakevarer.add("Brødskive u/pålegg        3,-");
        bakevarer.add("Kuvertbrød        5,- 8,- 10,-");
        bakevarer.add("Rundstykke m/pålegg        20,-");
        bakevarer.add("Enkle varianter        20,-/25,-");
        bakevarer.add("Ostebriks m/ost og skinke        28,-");
        bakevarer.add("Baguette m/ost og skinke        35,-");
        bakevarer.add("Baguette m/diverse pålegg        35,-");
        bakevarer.add("Baguette m/kylling        40,-");
        bakevarer.add("Baguette m/laks,reker,roastbiff        45,-");

        List<String> kaker = new ArrayList<String>();
        kaker.add("Vestlandslefse,småkaker        10,- 15,-");
        kaker.add("Cookies/brownie        20,-");
        kaker.add("Skillingsboller        20,-");
        kaker.add("Wienerbrød o.l.        20,-");
        kaker.add("Boller        10,-");
        kaker.add("Gulrotkake, muffins        20,-");
        kaker.add("Dessertkaker, bløtkaker o.l.        35,-");

        List<String> smaaanretninger = new ArrayList<String>();
        smaaanretninger.add("Salat/frukt etter vekt        13,90,- pr. hg");
        smaaanretninger.add("Salat kuvert        6,- 10,- 12,-");
        smaaanretninger.add("Hel frukt        6,- 2 for 10,-");
        smaaanretninger.add("Kuvert pålegg        6,- 8,-");
        smaaanretninger.add("Havregrøt        15,-");
        smaaanretninger.add("Frokostpakke, eget oppslag        25,-");
        smaaanretninger.add("Dobbeldekker        15,-");

        List<String> diverse = new ArrayList<String>();
        diverse.add("Kuvert margarin        3,-");
        diverse.add("Flatbrød        3,-");
        diverse.add("Majones/dressing        3,-/6,-");
        diverse.add("Suppe u/brød        Se oppslag");
        diverse.add("Yoghurt 175 ml        12,-");
        diverse.add("Duo/god morgen        19,-");
        diverse.add("Bygg-gryns lunsj        20,-");
        diverse.add("Yoghurt gresk variant        28,-");

        List<String> middaglunsj = new ArrayList<String>();
        middaglunsj.add("Middager        65,-");
        middaglunsj.add("Dagens varme lunsj        35,40,45,50,-");
        middaglunsj.add("Middagskort        580,-");
        middaglunsj.add("Dyrere middag betales med diff. mellom 65,- og pris");

        listDataChild.put(listDataHeader.get(0), kaffeprodukter);
        listDataChild.put(listDataHeader.get(1), drikkevarer);
        listDataChild.put(listDataHeader.get(2), bakevarer);
        listDataChild.put(listDataHeader.get(3), kaker);
        listDataChild.put(listDataHeader.get(4), smaaanretninger);
        listDataChild.put(listDataHeader.get(5), diverse);
        listDataChild.put(listDataHeader.get(6), middaglunsj);
    }
}
