package no.clap.higster;

import no.clap.higster.R;
import cn.pedant.SweetAlert.SweetAlertDialog;
import eu.inmite.android.lib.dialogs.ISimpleDialogListener;
import eu.inmite.android.lib.dialogs.SimpleDialogFragment;
import no.clap.higster.R;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.parse.ParsePush;


public class FindBooksActivity extends FragmentActivity implements ISimpleDialogListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_books);

        // Find the webview and the autocomplete textview
        final WebView web = (WebView)findViewById(R.id.webView);
        final AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.autocomplete_class);

        // Populate the classes with the string array containing classes
        String[] classes = getResources().getStringArray(R.array.classes);
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, classes);

        textView.setAdapter(adapter);

        // When a user presses a class from the autocomplete view, the webview is looking
        // for the HTML file.
        textView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();

                web.loadUrl("http://www.stud.hig.no/~120217/higster/books/" + item + ".html");
                
                // Removes keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_find_books, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // Send mail SimpleDialogFragment
        if (id == R.id.bookstore_contact) {
            SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                    .setTitle(R.string.bookstore_title).setMessage(R.string.bookstore_contact_info)
                    .setNegativeButtonText(R.string.close)
                    .setPositiveButtonText("Send mail").setRequestCode(1).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // For mail, new mail-intent
    @Override
    public void onPositiveButtonClicked(int requestCode) {
        if (requestCode == 1) {
            final Intent newmail = new Intent(android.content.Intent.ACTION_SEND);
            newmail.setType("plain/text");
            newmail.putExtra(android.content.Intent.EXTRA_EMAIL,
                    new String[] { "bok.gjovik@sopp.no" });
            newmail.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
            newmail.putExtra(android.content.Intent.EXTRA_TEXT, "");
            startActivity(Intent.createChooser(newmail, "Send mail..."));
        }
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {
    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {
    }

}
