package no.clap.higster;

import eu.inmite.android.lib.dialogs.SimpleDialogFragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

public class MapsActivity extends FragmentActivity {

    private GoogleMap mMap;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();
        mMap.setMyLocationEnabled(true);  // Shows current position
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.maps, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        LatLng hig = new LatLng(60.789237,10.681779);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hig,17));			// Moves camera to campus

		// Add marker to all the buildings on campus
        mMap.addMarker(new MarkerOptions().position(new LatLng(60.790107,10.683389)).title(getString(R.string.a_build)));
        mMap.addMarker(new MarkerOptions().position(new LatLng(60.790332, 10.682455)).title(getString(R.string.k_build)));
        mMap.addMarker(new MarkerOptions().position(new LatLng(60.789515,10.680749)).title(getString(R.string.b_build)));
        mMap.addMarker(new MarkerOptions().position(new LatLng(60.788447,10.680749)).title(getString(R.string.h_build)));
        mMap.addMarker(new MarkerOptions().position(new LatLng(60.78973, 10.682412)).title(getString(R.string.g_build))
                .snippet(getString(R.string.main_building))).showInfoWindow();
    }

    public void showInfo(MenuItem item) {
         Toast.makeText(getApplicationContext(), R.string.maps_info, Toast.LENGTH_LONG).show();
    }

    public void checkIn(MenuItem item) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle(R.string.what_room);

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                final String value = input.getText().toString().trim().toUpperCase();

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                if (value.length() > 0) {
                    sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.maps_share_room) + " " + value + ".");


                    ParseQuery<ParseObject> query = ParseQuery.getQuery("rooms");
                    query.whereEqualTo("room", value);
                    query.findInBackground(new FindCallback<ParseObject>() {
                        public void done(List<ParseObject> roomlist, ParseException e) {
                            if (e == null) {
                                if (roomlist.size() > 0) {
                                    // found the class
                                    ParseObject room = roomlist.remove(0);
                                    room.increment("count");
                                    room.saveInBackground();
                                }

                                else {
                                    ParseObject room = new ParseObject("rooms");
                                    room.put("room", value);
                                    room.put("count", 1);
                                    room.saveInBackground();
                                }

                            } else {
                                Log.d("score", "Error: " + e.getMessage());
                            }
                        }
                    });
                }
                else {
                    sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.maps_share));
                }
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();
    }

    public void mostPop(MenuItem item) {
        startLoading();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("rooms").orderByDescending("count");

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> roomlist, ParseException e) {
                stopLoading();
                StringBuilder output = new StringBuilder();
                output.append(getString(R.string.roomsoutput_start));

                if (e == null) {                                    // no errors
                    for (int i = 0; i < roomlist.size(); i++) {
                        ParseObject a = roomlist.remove(i);

                        output.append(a.get("room").toString()); output.append(" - ");
                        output.append(a.getNumber("count").toString()); output.append("\n");

                        roomlist.add(i, a);
                    }

                } else {
                    Log.d("rooms", "Error: " + e.getMessage());     // log the error
                }

                if (roomlist.size() == 0) {
                    output.append(getString(R.string.no_checkins));
                }

                generateFragment(output.toString());
            }
        });
    }

    private void generateFragment(String finaloutput) {
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager()).
                setTitle(R.string.maps_mostpop).setMessage(finaloutput).show();
    }

    public void startLoading() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getString(R.string.getting_data));
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(true);
        pDialog.show();
    }

    public void stopLoading() {
        pDialog.dismiss();
        pDialog = null;
    }

}

