package no.clap.higster;

import no.clap.higster.R;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import eu.inmite.android.lib.dialogs.SimpleDialogFragment;


public class MoreActivity extends FragmentActivity implements AdapterView.OnItemClickListener {

    ListView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more);

        listview = (ListView)findViewById(R.id.listView);
        listview.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
        Intent intent;
        String url;
        switch ((int) id) {
            case 0: url = "http://hig.no/it_tjenesten/rettleiingar/";
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent); break;
            case 1: url = "https://fronter.com/hig/";
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent); break;
            case 2: url = "https://www.studweb.no/as/WebObjects/studentweb2?inst=HiG";
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent); break;
            case 3: url = "http://fiber.hig.no/";
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent); break;
            case 4: url = "http://blog.hig.no/studentparlamentet/";
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent); break;
            case 5: url = "https://play.google.com/store/apps/details?id=no.clap.studrab";
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent); break;
            case 6: SimpleDialogFragment.createBuilder(this, getSupportFragmentManager()).
                        setTitle(R.string.about).setMessage(R.string.about_info).show();
                    break;
            case 7: SimpleDialogFragment.createBuilder(this, getSupportFragmentManager()).
                        setTitle(R.string.licenses).setMessage(R.string.licenses_info).show();
                    break;
            default: break;
        }
    }
}
