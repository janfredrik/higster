package no.clap.higster;

import no.clap.higster.R;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Toast;

// import com.parse.Parse;
// import com.parse.ParseAnalytics;
// import com.parse.ParseInstallation;
// import com.parse.ParsePush;
// import com.parse.PushService;

import java.util.Date;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import eu.inmite.android.lib.dialogs.SimpleDialogFragment;


public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

		// For notifications and data storage
        // Parse.initialize(this, "LLbpoz4GE3wrTZJPsQwRp9C3AgxRsW8uki0QqubJ", "WQOR89pmKgjZF3m0fH0t3rQbASrPmbNuvTvNZs0b");
        // ParseInstallation.getCurrentInstallation().saveInBackground();
    }

    public void startCafeteriaActivity(View v) {
        Intent intent = new Intent(this, CafeteriaActivity.class);
        startActivity(intent);					// Starts the CafeteriaActivity
    }

    public void startMapsActivity(View v){
        Intent map = new Intent(this, MapsActivity.class);
        startActivity(map);						// Starts the MapsActivity
    }

    public void startTimeEditActivity(View v){
        Intent te = new Intent(this, TimeEdit.class);
        startActivity(te);						// Starts the TimeEdit Activity
    }

    public void startFindBooksActivity(View v){
        Intent books = new Intent(this, FindBooksActivity.class);
        startActivity(books);
    }

    public void showOpeningHours(View v) {
        SimpleDialogFragment.createBuilder(this, getSupportFragmentManager()).
                setTitle(R.string.opening_title).setMessage(R.string.opening).show();
    }

    public void startMoreActivity(View v) {
        Intent more = new Intent(this, MoreActivity.class);
        startActivity(more);
    }

    public void calculateDiff() {
        SimpleDateFormat dfDate  = new SimpleDateFormat("dd/MM/yyyy");
        Date d = null;
        Date d1 = null;
        Calendar cal = Calendar.getInstance();
        try {
            d = dfDate.parse("19/12/2014");
            d1 = dfDate.parse(dfDate.format(cal.getTime()));//Returns 15/10/2012
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        int diffInDays = (int) ((d.getTime() - d1.getTime())/ (1000 * 60 * 60 * 24));
        Toast.makeText(getApplicationContext(), diffInDays, Toast.LENGTH_SHORT).show();
    }

}
