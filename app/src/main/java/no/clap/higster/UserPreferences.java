package no.clap.higster;

import no.clap.higster.R;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class UserPreferences {
    public static final String CLASS_NAME = "CLASS";
    public static final String INTERVAL = "INTERVAL";
    public static final String RUN = "RUN";
    private static final String APP_SHARED_PREFS = UserPreferences.class.getSimpleName(); //  Name of the file -.xml
    private SharedPreferences _sharedPrefs;
    private SharedPreferences.Editor _prefsEditor;

    public UserPreferences(Context context) {
        this._sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
        this._prefsEditor = _sharedPrefs.edit();
    }

    public String getDefaultClassName() {
        return _sharedPrefs.getString(CLASS_NAME, "");
    }

    public void saveDefaultClassName(String text) {
        _prefsEditor.putString(CLASS_NAME, text);
        _prefsEditor.commit();
    }

    public boolean getInterval() {
        return _sharedPrefs.getBoolean(INTERVAL, true);
    }

    public void saveInterval(boolean i) {
        _prefsEditor.putBoolean(INTERVAL, i);
        _prefsEditor.commit();
    }

    public boolean firstRun() { return _sharedPrefs.getBoolean(RUN, true); }

    public void saveFirstRunInfo(boolean i) {
        _prefsEditor.putBoolean(RUN, i);
        _prefsEditor.commit();
    }
}