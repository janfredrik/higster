package no.clap.higster;

import no.clap.higster.R;
import cn.pedant.SweetAlert.SweetAlertDialog;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class TimeEdit extends Activity {

    private UserPreferences userPrefs;
    Integer NO_OF_DAYS = 14;            // Timetable search interval
    AutoCompleteTextView searchField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_edit);
        userPrefs = new UserPreferences(getApplicationContext());

        final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioInterval);
        final Button searchButton = (Button) findViewById(R.id.search_button);
        searchField = (AutoCompleteTextView) findViewById(R.id.autocomplete_class);


        // Shows the default class in the search field
        if (userPrefs.getDefaultClassName().length() > 0) {
            searchField.setText(userPrefs.getDefaultClassName());
        }

        // If the user clicks search, it will create a Web View based on the search string
        searchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String classname = searchField.getText().toString().toUpperCase().trim();
                if (findClassValue(classname) != 0) {
                    createTimeEditWebView(classname);
                } else {
                    displayClassNameError(classname);
                }
            }
        });

        // If the user wants to click on a item from the list. Initiating the list, and if the
        // user clicks on a item, a Web View based on the item (class) will be generated
        String[] classes = getResources().getStringArray(R.array.classes);
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, classes);

        searchField.setAdapter(adapter);

        searchField.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();

                createTimeEditWebView(item);

                // Remove the keyboard from the screen
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchField.getWindowToken(), 0);
            }
        });

        // Sets the radio button as true (standard) = 14 days or false = 30 days.
        if (userPrefs.getInterval() == false) {
            radioGroup.check(R.id.rb_30);       // Set button 2 as checked
        }

        // If the user changes the Radio Button, the interval on the timetable will be changed.
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int index = radioGroup.indexOfChild(findViewById(radioGroup.getCheckedRadioButtonId()));

                if (index == 0) {
                    userPrefs.saveInterval(true);
                    NO_OF_DAYS = 14;
                    Toast.makeText(getApplicationContext(), "Changed to 14 days.", Toast.LENGTH_SHORT).show();
                } else if (index == 1) {
                    userPrefs.saveInterval(false);
                    NO_OF_DAYS = 30;
                    Toast.makeText(getApplicationContext(), "Changed to 30 days.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_time_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.changeClass) {
            final EditText input = new EditText(this);
            input.setText(userPrefs.getDefaultClassName());

            // Builds a Dialog where the user can enter a standard class.
            new AlertDialog.Builder(TimeEdit.this)
                .setTitle(R.string.change_class)
                .setView(input)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String classname = input.getText().toString().toUpperCase().trim();
                        if (findClassValue(classname) != 0) {
                            userPrefs.saveDefaultClassName(classname);
                            searchField.setText(classname);
                            showSuccessMessage(classname);
                        } else {
                            displayClassNameError(classname);
                        }
                    }
                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Do nothing.
                }
            }).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void displayClassNameError(String classname) {
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        .setTitleText(getString(R.string.error))
        .setContentText(getString(R.string.no_result) + " " + classname)
        .show();
    }

    private void showSuccessMessage(String classname) {
        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(getString(R.string.success))
                .setContentText(classname + " " + getString(R.string.set_as_default))
                .show();
    }

    // Creates the WebView that shows the timetable for the user.
    private void createTimeEditWebView(String classname) {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");         // For Date-handling
        Calendar cal = Calendar.getInstance();
        WebView webview = new WebView(TimeEdit.this);                   // WebView for TimeEdit

        String startDate = df.format(cal.getTime());                    // Get current date
        cal.add(Calendar.DAY_OF_YEAR, NO_OF_DAYS);                      // Add NO_OF_DAYS (14/30)
        String endDate = df.format(cal.getTime());                      // Generate end date


        // Generate final URL
        String URL = "https://web.timeedit.se/hig_no/db1/open/r.html?sid=3&h=t&p="
                + startDate + ".x%2C" + endDate + ".x&objects="
                + findClassValue(classname) + ".182&ox=0&types=0&fe=0";

        setContentView(webview);
        webview.setWebViewClient(new Callback());
        webview.loadUrl(URL);
    }

    // Combination Switch + String is not working. We have to use if/else..
    private Integer findClassValue(String classname) {
        Integer classnumber = 0;

        if (classname.equals("12HÅRMEDLED")) classnumber = 161545;
        else if (classname.equals("12HÅRGISA")) classnumber = 161546;
        else if (classname.equals("12HÅRLANDD")) classnumber = 161547;
        else if (classname.equals("12HÅRMEDPRODA")) classnumber = 161548;
        else if (classname.equals("12HÅRMITA")) classnumber = 161549;
        else if (classname.equals("12HÅRØKLEDA")) classnumber = 161550;
        else if (classname.equals("12HBDRA")) classnumber = 161551;
        else if (classname.equals("12HBGEOA")) classnumber = 161552;
        else if (classname.equals("12HBIBYGA")) classnumber = 161553;
        else if (classname.equals("12HBIBYGT")) classnumber = 161554;
        else if (classname.equals("12HBIBYGLANA")) classnumber = 161555;
        else if (classname.equals("12HBIDATA")) classnumber = 161556;
        else if (classname.equals("12HBIDATB")) classnumber = 161557;
        else if (classname.equals("12HBIDATT")) classnumber = 161558;
        else if (classname.equals("12HBIELEA")) classnumber = 161559;
        else if (classname.equals("12HBIELEB")) classnumber = 161560;
        else if (classname.equals("12HBIELET")) classnumber = 161561;
        else if (classname.equals("12HBIMASA")) classnumber = 161562;
        else if (classname.equals("12HBIMASB")) classnumber = 161563;
        else if (classname.equals("12HBIMAST")) classnumber = 161564;
        else if (classname.equals("12HBISA")) classnumber = 161565;
        else if (classname.equals("12HBMEDA")) classnumber = 161566;
        else if (classname.equals("12HBMPA")) classnumber = 161567;
        else if (classname.equals("12HBMIA")) classnumber = 161568;
        else if (classname.equals("12HBPUA")) classnumber = 161569;
        else if (classname.equals("12HBRADA")) classnumber = 161570;
        else if (classname.equals("12HBSPA")) classnumber = 161571;
        else if (classname.equals("12HBSPLD")) classnumber = 161572;
        else if (classname.equals("12HBSPLHA")) classnumber = 161573;
        else if (classname.equals("12HBTEKDA")) classnumber = 161574;
        else if (classname.equals("12HBØKLEDA")) classnumber = 161575;
        else if (classname.equals("12HMBMD")) classnumber = 161576;
        else if (classname.equals("12HMISA")) classnumber = 161578;
        else if (classname.equals("12HMACS")) classnumber = 161579;
        else if (classname.equals("12HÅRTEKA")) classnumber = 161580;
        else if (classname.equals("12HBIBYGB")) classnumber = 161581;
        else if (classname.equals("12HBWUA")) classnumber = 161583;
        else if (classname.equals("12HBFENA")) classnumber = 161586;
        else if (classname.equals("12HMKLIN1")) classnumber = 161610;
        else if (classname.equals("12HMKLIN3")) classnumber = 161611;
        else if (classname.equals("12HMSUMA")) classnumber = 161615;
        else if (classname.equals("12HVVEILA")) classnumber = 161662;
        else if (classname.equals("13VAIO")) classnumber = 161712;
        else if (classname.equals("13VMKLINSYKA")) classnumber = 161713;
        else if (classname.equals("12VMHPCC")) classnumber = 161714;
        else if (classname.equals("12HBSPLHB")) classnumber = 161720;
        else if (classname.equals("12HBSPLHA1")) classnumber = 161723;
        else if (classname.equals("12HBSPLHA2")) classnumber = 161735;
        else if (classname.equals("12HBSPLHA3")) classnumber = 161736;
        else if (classname.equals("12HBSPLHA4")) classnumber = 161737;
        else if (classname.equals("12HBSPLHA5")) classnumber = 161738;
        else if (classname.equals("12HBSPLHA6")) classnumber = 161739;
        else if (classname.equals("12HBSPLHA7")) classnumber = 161740;
        else if (classname.equals("12HBSPLHA8")) classnumber = 161741;
        else if (classname.equals("12HBSPLHB09")) classnumber = 161742;
        else if (classname.equals("12HBSPLHB10")) classnumber = 161743;
        else if (classname.equals("12HBSPLHB11")) classnumber = 161744;
        else if (classname.equals("12HBSPLHB12")) classnumber = 161745;
        else if (classname.equals("12HBSPLHB13")) classnumber = 161746;
        else if (classname.equals("12HBSPLHB14")) classnumber = 161747;
        else if (classname.equals("12HBSPLHB15")) classnumber = 161748;
        else if (classname.equals("12HBSPLHB16")) classnumber = 161749;
        else if (classname.equals("13VPREHOS1")) classnumber = 161750;
        else if (classname.equals("13HBSPLH")) classnumber = 161767;
        else if (classname.equals("12HBSPLDG")) classnumber = 161771;
        else if (classname.equals("12HBSPLDH")) classnumber = 161772;
        else if (classname.equals("12HBSPLDN")) classnumber = 161773;
        else if (classname.equals("12HBSPLDV")) classnumber = 161774;
        else if (classname.equals("13HBERGA")) classnumber = 161775;
        else if (classname.equals("13HBFENA")) classnumber = 161776;
        else if (classname.equals("13HBGEOA")) classnumber = 161777;
        else if (classname.equals("13HBIBYGA")) classnumber = 161778;
        else if (classname.equals("13HBIBYGB")) classnumber = 161779;
        else if (classname.equals("13HBIBYGT")) classnumber = 161780;
        else if (classname.equals("13HBIDATA")) classnumber = 161781;
        else if (classname.equals("13HBIDATB")) classnumber = 161782;
        else if (classname.equals("13HBIDATT")) classnumber = 161783;
        else if (classname.equals("13HBIELEA")) classnumber = 161784;
        else if (classname.equals("13HBIELEB")) classnumber = 161785;
        else if (classname.equals("13HBIELET")) classnumber = 161786;
        else if (classname.equals("13HBIMASA")) classnumber = 161787;
        else if (classname.equals("13HBIMASB")) classnumber = 161788;
        else if (classname.equals("13HBIMAST")) classnumber = 161789;
        else if (classname.equals("13HBISA")) classnumber = 161790;
        else if (classname.equals("13HBMEDA")) classnumber = 161791;
        else if (classname.equals("13HBMIA")) classnumber = 161792;
        else if (classname.equals("13HBMPA")) classnumber = 161793;
        else if (classname.equals("13HBPUA")) classnumber = 161794;
        else if (classname.equals("13HBRADA")) classnumber = 161795;
        else if (classname.equals("13HBSPA")) classnumber = 161796;
        else if (classname.equals("13HBSPLD")) classnumber = 161797;
        else if (classname.equals("13HBSPLHA")) classnumber = 161798;
        else if (classname.equals("13HBSPLHB")) classnumber = 161799;
        else if (classname.equals("13HBTEKDA")) classnumber = 161800;
        else if (classname.equals("13HBWUA")) classnumber = 161801;
        else if (classname.equals("13HBØKLEDA")) classnumber = 161802;
        else if (classname.equals("13HMACSA")) classnumber = 161803;
        else if (classname.equals("13HMIXDA")) classnumber = 161804;
        else if (classname.equals("13HMGERONTA")) classnumber = 161805;
        else if (classname.equals("13HMHPCCA")) classnumber = 161806;
        else if (classname.equals("13HMISA")) classnumber = 161807;
        else if (classname.equals("13HCIMET")) classnumber = 161808;
        else if (classname.equals("13HMSUMA")) classnumber = 161809;
        else if (classname.equals("13HVPALL")) classnumber = 161810;
        else if (classname.equals("13HVVEIL")) classnumber = 161811;
        else if (classname.equals("13HÅRGISA")) classnumber = 161812;
        else if (classname.equals("13HÅRLANDA")) classnumber = 161813;
        else if (classname.equals("13HÅRMEDLEDA")) classnumber = 161814;
        else if (classname.equals("13HÅRMEDPROD")) classnumber = 161815;
        else if (classname.equals("13HÅRMITA")) classnumber = 161816;
        else if (classname.equals("13HÅRTEKA")) classnumber = 161817;
        else if (classname.equals("13HÅRØKLEDA")) classnumber = 161818;
        else if (classname.equals("12HBIBYGP")) classnumber = 161823;
        else if (classname.equals("12HBIBYGK")) classnumber = 161824;
        else if (classname.equals("13HBDRA")) classnumber = 161832;
        else if (classname.equals("12HBIMASL")) classnumber = 161840;
        else if (classname.equals("12HCIMET")) classnumber = 161885;
        else if (classname.equals("12HM3D")) classnumber = 161892;
        else if (classname.equals("13HBIBYGF")) classnumber = 161897;
        else if (classname.equals("13HBIELEF")) classnumber = 161898;
        else if (classname.equals("13HBIMASF")) classnumber = 161899;
        else if (classname.equals("13HBSPLH gruppe")) classnumber = 161901;
        else if (classname.equals("12VMKLINSYK")) classnumber = 161902;
        else if (classname.equals("13VVEILDN")) classnumber = 161904;
        else if (classname.equals("13HÅRLANDD")) classnumber = 161912;
        else if (classname.equals("13HBERG gruppe")) classnumber = 161926;
        else if (classname.equals("13HVKLED")) classnumber = 161939;
        else if (classname.equals("13HÅRGIS-F")) classnumber = 161947;
        else if (classname.equals("12HMIXDA")) classnumber = 162016;
        else if (classname.equals("12HBIELEB")) classnumber = 162022;
        else if (classname.equals("12HBIELEC")) classnumber = 162023;
        else if (classname.equals("12HBIELED")) classnumber = 162024;
        else if (classname.equals("14VAIO")) classnumber = 162035;
        else if (classname.equals("14VINTEN")) classnumber = 162036;
        else if (classname.equals("14VANEST")) classnumber = 162037;
        else if (classname.equals("14VOPERA")) classnumber = 162038;
        else if (classname.equals("12HMISDA")) classnumber = 162044;
        else if (classname.equals("13hmisda")) classnumber = 162045;
        else if (classname.equals("14HBDRA")) classnumber = 162090;
        else if (classname.equals("14HBERGA")) classnumber = 162091;
        else if (classname.equals("14HBFENA")) classnumber = 162092;
        else if (classname.equals("14HBGEOA")) classnumber = 162093;
        else if (classname.equals("14HBIBYGA")) classnumber = 162094;
        else if (classname.equals("14HBIBYGB")) classnumber = 162095;
        else if (classname.equals("14HBIBYGF")) classnumber = 162096;
        else if (classname.equals("14HBIBYGT")) classnumber = 162097;
        else if (classname.equals("14HBIDATA")) classnumber = 162098;
        else if (classname.equals("14HBIDATB")) classnumber = 162099;
        else if (classname.equals("14HBIDATT")) classnumber = 162100;
        else if (classname.equals("14HBIELEA")) classnumber = 162101;
        else if (classname.equals("14HBIELEB")) classnumber = 162102;
        else if (classname.equals("14HBIELET")) classnumber = 162103;
        else if (classname.equals("14HBIMASA")) classnumber = 162104;
        else if (classname.equals("14HBIMASB")) classnumber = 162105;
        else if (classname.equals("14HBIMASF")) classnumber = 162106;
        else if (classname.equals("14HBIMAST")) classnumber = 162107;
        else if (classname.equals("14HBISA")) classnumber = 162108;
        else if (classname.equals("14HBMEDA")) classnumber = 162109;
        else if (classname.equals("14HBMIA")) classnumber = 162110;
        else if (classname.equals("14HBMPA")) classnumber = 162111;
        else if (classname.equals("14HBPUA")) classnumber = 162112;
        else if (classname.equals("14HBRADA")) classnumber = 162113;
        else if (classname.equals("14HBSPA")) classnumber = 162114;
        else if (classname.equals("14HBSPLHA")) classnumber = 162115;
        else if (classname.equals("14HBTEKA")) classnumber = 162116;
        else if (classname.equals("14HBWUA")) classnumber = 162117;
        else if (classname.equals("14HBØKLEDA")) classnumber = 162118;
        else if (classname.equals("14HCIMET")) classnumber = 162119;
        else if (classname.equals("14HMACSA")) classnumber = 162120;
        else if (classname.equals("14HMGERONTA")) classnumber = 162121;
        else if (classname.equals("14HMHPCCA")) classnumber = 162122;
        else if (classname.equals("14HMISA")) classnumber = 162123;
        else if (classname.equals("14HMISDA")) classnumber = 162124;
        else if (classname.equals("14HMIXDA")) classnumber = 162125;
        else if (classname.equals("14HMSUMA")) classnumber = 162126;
        else if (classname.equals("14HVPALL")) classnumber = 162127;
        else if (classname.equals("14HVVEIL")) classnumber = 162128;
        else if (classname.equals("14HÅRGISA")) classnumber = 162129;
        else if (classname.equals("14HÅRLANDA")) classnumber = 162130;
        else if (classname.equals("14HÅRLANDD")) classnumber = 162131;
        else if (classname.equals("14HÅRMEDLEDA")) classnumber = 162132;
        else if (classname.equals("14HÅRMEDPROD")) classnumber = 162133;
        else if (classname.equals("14HÅRINFA")) classnumber = 162134;
        else if (classname.equals("14HÅRTEKA")) classnumber = 162135;
        else if (classname.equals("14HÅRØKLEDA")) classnumber = 162136;
        else if (classname.equals("14HBDRB")) classnumber = 162137;
        else if (classname.equals("13HBIBYGK")) classnumber = 162138;
        else if (classname.equals("13HBIBYGP")) classnumber = 162139;
        else if (classname.equals("13HBIBYGLANA")) classnumber = 162140;

        return classnumber;
    }

    // This will make external links open in the app
    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return (false);
        }
    }
}
