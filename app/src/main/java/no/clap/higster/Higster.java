package no.clap.higster;

import android.app.Application;
import android.content.res.Configuration;

import com.parse.Parse;
import com.parse.ParseInstallation;

/**
 * Created by janfredrik on 06.12.2014.
 */
public class Higster extends Application {

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(this, "LLbpoz4GE3wrTZJPsQwRp9C3AgxRsW8uki0QqubJ", "WQOR89pmKgjZF3m0fH0t3rQbASrPmbNuvTvNZs0b");
        ParseInstallation.getCurrentInstallation().saveInBackground();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}

