package no.clap.higster;

import no.clap.higster.R;
import cn.pedant.SweetAlert.SweetAlertDialog;
import eu.inmite.android.lib.dialogs.ISimpleDialogListener;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParsePush;
import com.parse.PushService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;

import eu.inmite.android.lib.dialogs.SimpleDialogFragment;


public class CafeteriaActivity extends FragmentActivity implements ISimpleDialogListener {
    // Dinner list items
    ListView list;
    TextView day;
    TextView food;
	
	// ArrayList, URL to JSON-file and JSON node names
    ArrayList<HashMap<String, String>> dinnerlist = new ArrayList<HashMap<String, String>>();
    private static String url = "http://www.stud.hig.no/~120217/higster/dinner.json";
	
    JSONArray dinnerjson = null;
    private static final String TAG_DINNER = "dinner";
    private static final String TAG_DAY = "day";
    private static final String TAG_FOOD = "food";

    private UserPreferences userPrefs;                  // Push notifications


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cafeteria);

        // If it's the first time the users run the app, we want to turn on
        // new dinner list notifications. The user can turn this on/off in the ActionBar
        userPrefs = new UserPreferences(getApplicationContext());
        if (userPrefs.firstRun()) {
            ParsePush.subscribeInBackground("Dinner");
            userPrefs.saveFirstRunInfo(false);
        }

        if (haveNetworkConnection()) {                     // Check for network connection
            new JSONParse().execute();                     // Get the JSON with dinner list
        }
        else {
            displayError();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cafeteria, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        
		// Displays a Dialog with contact information
        if (id == R.id.cafeteria_contact) {
            SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                    .setTitle(R.string.cafeteria_contact_title)
                    .setMessage(R.string.cafeteria_contact_info)
                    .setNegativeButtonText(R.string.close)
                    .setPositiveButtonText("Send mail").setRequestCode(1).show();
            return true;
        }

        // Displays a Dialog with a Yes or No (Notifications)
        if (id == R.id.notifications) {
            SimpleDialogFragment.createBuilder(this, getSupportFragmentManager())
                    .setTitle(R.string.notifications).setMessage(R.string.notifications_desc)
                    .setPositiveButtonText(R.string.yes).setNegativeButtonText(R.string.no)
                    .setRequestCode(2).show();
            return true;
        }

		// Opens the facebook app/web page with the SOPP facebook page.
        if (id == R.id.cafeteria_facebook) {
            String uri = "fb://page/153122051413387";                       // Facebook-app URL to SOPP
            boolean installed = isFacebookInstalled("com.facebook.katana"); // Check if user got FB-app
            if (installed) {                                      // If installed, open the app
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }                                                     // else, open the browser
            else {
                uri = "http://www.facebook.com/pages/SOPP-Studentsamskipnaden-i-Oppland/153122051413387/";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
            return true;
        }

		// Displays a Dialog with cafeteria prices
        if (id == R.id.cafeteria_prices) {
            Intent pricelist = new Intent(this, PriceListActivity.class);
            startActivity(pricelist);						// Starts the PriceListActivity
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Displays a Dialog-error
    private void displayError() {
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        .setTitleText(getString(R.string.error))
                .setContentText(getString(R.string.dinner_error))
                .show();
    }

    // Handle the button clicks in the Dialogs. Send mail and Push-settings
    @Override
    public void onPositiveButtonClicked(int requestCode) {
        if (requestCode == 1) {
            final Intent newmail = new Intent(android.content.Intent.ACTION_SEND);
            newmail.setType("plain/text");
            newmail.putExtra(android.content.Intent.EXTRA_EMAIL,
                    new String[] { "berit.grina.wold@sopp.no" });
            newmail.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
            newmail.putExtra(android.content.Intent.EXTRA_TEXT, "");
            startActivity(Intent.createChooser(newmail, "Send mail..."));
        }

        if (requestCode == 2) {
            ParsePush.subscribeInBackground("Dinner");
            new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText(getString(R.string.notif_on))
                    .show();
        }
    }

    @Override
    public void onNegativeButtonClicked(int requestCode) {
        if (requestCode == 2) {
            ParsePush.unsubscribeInBackground("Dinner");
            new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText(getString(R.string.notif_off))
                    .show();
        }
    }

    @Override
    public void onNeutralButtonClicked(int requestCode) {
    }

    public void updateDinnerList(MenuItem item) {
        if (haveNetworkConnection()) {                  // Check for network connection
            list.invalidateViews();                     // Clear List items and
            dinnerlist.clear();                         // the ArrayList
            new JSONParse().execute();					// Execute JSONParse
        }
        else {
            displayError();
        }
    }

    private boolean isFacebookInstalled(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;           // Returns true/false if the Facebook-app is installed.
    }

    // Checks for network connection - so that the json can be fetched
    private boolean haveNetworkConnection() {
        boolean haveWifi = false;
        boolean haveMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveMobile = true;
        }
        return haveWifi || haveMobile;
    }

    // Below: JSONparsing and ListView initialization for the external "dinner" JSON-file
    // Source: http://www.learn2crack.com/2013/11/listview-from-json-example.html
    private class JSONParse extends AsyncTask<String, String, JSONObject> {
        private ProgressDialog pDialog;
        
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            day = (TextView)findViewById(R.id.day);
            food = (TextView)findViewById(R.id.food);
            pDialog = new ProgressDialog(CafeteriaActivity.this);
            pDialog.setMessage(CafeteriaActivity.this.getString(R.string.getting_data));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();                 // Show loading-dialog
        }
        
        @Override
        protected JSONObject doInBackground(String... args) {
            JSONParser jParser = new JSONParser();
            // Getting JSON from URL
            JSONObject json = jParser.getJSONFromUrl(url);
            return json;
        }
        
        @Override
        protected void onPostExecute(JSONObject json) {
            pDialog.dismiss();              // Remove loading-dialog
            try {
                // Getting JSON Array from URL
                dinnerjson = json.getJSONArray(TAG_DINNER);
                for(int i = 0; i < dinnerjson.length(); i++){
                    JSONObject c = dinnerjson.getJSONObject(i);
                    // Storing JSON item in a Variable
                    String day = c.getString(TAG_DAY);
                    String food = c.getString(TAG_FOOD);
                    // Adding value HashMap key => value
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_DAY, day);
                    map.put(TAG_FOOD, food);
                    dinnerlist.add(map);
                    list=(ListView)findViewById(R.id.dinnerList);
                    ListAdapter adapter = new SimpleAdapter(CafeteriaActivity.this, dinnerlist,
                            R.layout.list_v,
                            new String[] { TAG_DAY, TAG_FOOD }, new int[] {
                    R.id.day, R.id.food});
                    list.setAdapter(adapter);
                }
            } catch (JSONException e) {
                e.printStackTrace();
              }
        }
    }
}
